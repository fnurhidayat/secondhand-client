import axios from "axios";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import cashier from "../assets/img/Rectangle-131.png";

import "../assets/signup_login.css";

function Signup() {
  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorPassword, setErrorPassword] = useState(false);
  const [errorEmail, setErrorEmail] = useState(false);
  const [validForm, setValidForm] = useState(false);

  const register = async (event) => {
    event.preventDefault();

    if (password.length < 8) {
      setErrorPassword(true);

      return;
    }

    setErrorPassword(false);

    try {
      const response = await axios.post("/api/registration", {
        full_name: fullName,
        email,
        password,
      });

      const data = response.data;

      if (data.status) {
        setValidForm(true);
        setErrorEmail(false);
        setFullName("");
        setEmail("");
        setPassword("");
      }
    } catch (error) {
      console.log(error);
      setErrorEmail(true);
      setValidForm(false);
    }
  };

  return (
    <div className="App">
      <div className="row">
        <div className="col-sm-6 d-none d-sm-block position-relative">
          <img
            src={cashier}
            alt=""
            width={"100%"}
            height="100%"
            style={{ objectFit: "cover" }}
          />
          <h2 className="brand">
            Second
            <br />
            Hand.
          </h2>
        </div>
        <div className="col-sm-6 form-register">
          <form onSubmit={register}>
            <h2>
              <strong>Daftar</strong>
            </h2>
            {(validForm || errorEmail) && (
              <div
                className={`alert ${
                  errorEmail ? "alert-danger" : "alert-success"
                } alert-dismissible fade show`}
                role="alert"
              >
                <strong>
                  {errorEmail
                    ? "Email sudah digunakan"
                    : "Silahkan verifikasi email agar dapat menggunakan layanan kami"}
                </strong>
                <button
                  type="button"
                  className="close"
                  data-dismiss="alert"
                  aria-label="Close"
                  onClick={() => {
                    setValidForm(false);
                    setErrorEmail(false);
                  }}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            )}
            <div className="form-group">
              <label htmlFor="inputAddress">Nama</label>
              <input
                type="text"
                className="form-control"
                id="inputAddress"
                placeholder="Nama Lengkap"
                onChange={(e) => setFullName(e.target.value)}
                value={fullName}
                required
              />
            </div>
            <div className="form-group ">
              <label htmlFor="exampleInputEmail1 ">Email address</label>
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                placeholder="Contoh: johndee@gmail.com"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
                required
              />
            </div>
            <div className="form-group ">
              <label htmlFor="exampleInputPassword1 ">Password</label>
              <input
                type="password"
                className="form-control "
                id="exampleInputPassword1 "
                placeholder="Masukkan password"
                onChange={(e) => setPassword(e.target.value)}
                value={password}
                required
              />
              <div
                className={`invalid-feedback ${errorPassword ? "active" : ""}`}
              >
                Password minimal 8 karakter.
              </div>
            </div>
            <button type="submit " className="btn btn-primary w-100">
              Daftar
            </button>
            <p className="mt-4 text-center cta">
              <span>Sudah punya akun?</span>{" "}
              <Link to={"/login"}>Masuk di sini</Link>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Signup;
