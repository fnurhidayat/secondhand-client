import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";
import { createRef } from "react";
import { Alert, Button, Spinner } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { getCategories } from "../app/features/categoriesSlice";
import { selectedProduct } from "../app/features/productsSlice";
import "../assets/infoproduk.css";
import "../assets/style2.css";

const InfoProduk = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const inputFileEl = createRef();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const { selected: product } = useSelector((state) => state.products);
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [category, setCategory] = useState("");
  const [description, setDescription] = useState("");
  const [files, setFiles] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const categories = useSelector((state) => state.categories);

  const publishProduct = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      const productForm = new FormData();
      productForm.append("name", name);
      productForm.append("merchant_id", user.user.id);
      productForm.append("price", price);
      productForm.append("category_id", category);
      productForm.append("description", description);
      productForm.append("status", "available");
      if (files) {
        Object.keys(files).forEach((file) => {
          productForm.append("products", files[file]);
        });
      }

      const response = product?.id
        ? await axios.put(`/api/product/${product.id}`, productForm)
        : await axios.post("/api/product", productForm);

      if (response.data.status) {
        navigate(location.state ? location.state : "/");
        setIsLoading(false);
      } else {
        setShowAlert(true);
        setIsLoading(false);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const previewProduct = async () => {
    setIsLoading(true);

    try {
      const productForm = new FormData();
      productForm.append("name", name);
      productForm.append("merchant_id", user.user.id);
      productForm.append("price", price);
      productForm.append("category_id", category);
      productForm.append("description", description);
      productForm.append("status", "not available");
      if (files) {
        Object.keys(files).forEach((file) => {
          productForm.append("products", files[file]);
        });
      }

      const response = await axios.post("/api/product", productForm);

      if (response.data.status) {
        navigate(location.state ? location.state : "/");
        setIsLoading(false);
      } else {
        setShowAlert(true);
        setIsLoading(false);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (product) {
      setName(product?.name);
      setPrice(product?.price);
      setCategory(product?.category.id);
      setDescription(product?.description);
    }
  }, [product]);

  useEffect(() => {
    dispatch(getCategories());
  }, [dispatch]);

  return (
    <>
      <div className="container" style={{ position: "relative" }}>
        <div className="row mt-5">
          <div className="col-md-3 d-flex justify-content-end">
            <Link
              to={location.state ? location.state : "/"}
              onClick={() => dispatch(selectedProduct(null))}
            >
              <FontAwesomeIcon icon="fa-arrow-left" />
            </Link>
          </div>

          <form onSubmit={publishProduct} action="#" className="col-md-6">
            <div className="col-md mb-3">
              <label htmlFor="nm_produk" className="form-label">
                Nama Produk
              </label>

              <input
                type="type"
                className="form-control"
                id="nm_produk"
                placeholder="Nama Produk"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>

            <div className="col-md mb-3">
              <label htmlFor="harga_produk" className="form-label">
                Harga Produk
              </label>

              <input
                type="type"
                className="form-control"
                id="harga_produk"
                placeholder="Rp 0,00"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </div>

            <div className="col-md mb-3">
              <label htmlFor="kategori" className="form-label">
                Kategori
              </label>

              <select
                className="form-control"
                id="kategori"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              >
                <option>Pilih Kategori</option>
                {categories.status === "success" &&
                  categories.categories.data.map((arr) => (
                    <option key={"kategori-" + arr.name} value={arr.id}>
                      {arr.name}
                    </option>
                  ))}
              </select>
            </div>

            <div className="col-md mb-3">
              <label htmlFor="deskripsi" className="form-label">
                Deskripsi
              </label>

              <textarea
                className="form-control"
                id="deskripsi"
                rows="3"
                placeholder="Contoh: Jalan Ikan Hiu 33"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              ></textarea>
            </div>

            <div className="col-md mb-3">
              <label htmlFor="foto" className="form-label">
                Foto Produk
              </label>
              <br />

              <input
                type={"file"}
                hidden
                multiple
                ref={inputFileEl}
                name="products"
                onChange={(e) => setFiles(e.target.files)}
              />
              <div className="product-image-wrapper">
                {files &&
                  Object.keys(files).map((key) => (
                    <div className="product-images" key={"image-" + key}>
                      <img
                        src={URL.createObjectURL(files[key])}
                        alt="Gambar Produk"
                        style={{ objectFit: "contain" }}
                      />
                    </div>
                  ))}
              </div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="80"
                height="80"
                fill="currentColor"
                className="bi bi-plus-square-dotted mt-2"
                viewBox="0 0 16 16"
                onClick={() => inputFileEl.current.click()}
              >
                <path d="M2.5 0c-.166 0-.33.016-.487.048l.194.98A1.51 1.51 0 0 1 2.5 1h.458V0H2.5zm2.292 0h-.917v1h.917V0zm1.833 0h-.917v1h.917V0zm1.833 0h-.916v1h.916V0zm1.834 0h-.917v1h.917V0zm1.833 0h-.917v1h.917V0zM13.5 0h-.458v1h.458c.1 0 .199.01.293.029l.194-.981A2.51 2.51 0 0 0 13.5 0zm2.079 1.11a2.511 2.511 0 0 0-.69-.689l-.556.831c.164.11.305.251.415.415l.83-.556zM1.11.421a2.511 2.511 0 0 0-.689.69l.831.556c.11-.164.251-.305.415-.415L1.11.422zM16 2.5c0-.166-.016-.33-.048-.487l-.98.194c.018.094.028.192.028.293v.458h1V2.5zM.048 2.013A2.51 2.51 0 0 0 0 2.5v.458h1V2.5c0-.1.01-.199.029-.293l-.981-.194zM0 3.875v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zM0 5.708v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zM0 7.542v.916h1v-.916H0zm15 .916h1v-.916h-1v.916zM0 9.375v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zm-16 .916v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zm-16 .917v.458c0 .166.016.33.048.487l.98-.194A1.51 1.51 0 0 1 1 13.5v-.458H0zm16 .458v-.458h-1v.458c0 .1-.01.199-.029.293l.981.194c.032-.158.048-.32.048-.487zM.421 14.89c.183.272.417.506.69.689l.556-.831a1.51 1.51 0 0 1-.415-.415l-.83.556zm14.469.689c.272-.183.506-.417.689-.69l-.831-.556c-.11.164-.251.305-.415.415l.556.83zm-12.877.373c.158.032.32.048.487.048h.458v-1H2.5c-.1 0-.199-.01-.293-.029l-.194.981zM13.5 16c.166 0 .33-.016.487-.048l-.194-.98A1.51 1.51 0 0 1 13.5 15h-.458v1h.458zm-9.625 0h.917v-1h-.917v1zm1.833 0h.917v-1h-.917v1zm1.834-1v1h.916v-1h-.916zm1.833 1h.917v-1h-.917v1zm1.833 0h.917v-1h-.917v1zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z" />
              </svg>
            </div>

            <div className="row">
              {!product?.id && (
                <div className="col-md mb-3 d-grid">
                  <Button
                    onClick={previewProduct}
                    variant="outline-primary"
                    disabled={isLoading}
                  >
                    {isLoading ? (
                      <>
                        <Spinner
                          as="span"
                          animation="border"
                          size="sm"
                          role="status"
                          aria-hidden="true"
                        />
                        <span className="visually-hidden">Loading...</span>
                      </>
                    ) : (
                      "Preview"
                    )}
                  </Button>
                </div>
              )}

              <div className="col-md mb-3 d-grid">
                <Button type="submit" variant="primary" disabled={isLoading}>
                  {isLoading ? (
                    <>
                      <Spinner
                        as="span"
                        animation="border"
                        size="sm"
                        role="status"
                        aria-hidden="true"
                      />
                      <span className="visually-hidden">Loading...</span>
                    </>
                  ) : (
                    "Terbitkan"
                  )}
                </Button>
              </div>
            </div>
          </form>
        </div>
        <Alert
          show={showAlert}
          variant={"danger"}
          className="position-absolute top-0 start-50 translate-middle"
          style={{ zIndex: 10 }}
        >
          <p>
            Tidak bisa menambahkan produk, karena sudah lebih dari 4.{" "}
            <strong>Silahkan hapus produk</strong>
          </p>
          <FontAwesomeIcon
            icon="fa-times"
            style={{ cursor: "pointer" }}
            onClick={() => setShowAlert(false)}
          />
        </Alert>
      </div>
    </>
  );
};

export default InfoProduk;
