import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import axios from "axios";
import { Provider } from "react-redux";
import { store } from "./app/store";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faAngleLeft,
  faAngleRight,
  faArrowLeft,
  faBell,
  faImage,
  faList,
  faPhone,
  faPlus,
  faSearch,
  faSignIn,
  faTimes,
  faUser,
  faUserAlt,
} from "@fortawesome/free-solid-svg-icons";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Profile from "./pages/Profile";
import ProductPage from "./pages/ProductPage";
import InfoProduk from "./pages/InfoProduk";
import Signup from "./pages/signup";
import Login from "./pages/login";
import Home from "./pages/home";
import ProtectedRoute from "./components/protectedRoute";
import ListProduct from "./pages/listProduct";
import InfoPenawar from "./pages/infoPenawar";
import Verification from "./pages/verification";

const env = process.env.NODE_ENV || "development";

export const baseURL =
  process.env.REACT_APP_BASE_URL || "http://localhost:3000";

axios.defaults.baseURL = baseURL;

library.add(
  faSearch,
  faPlus,
  faSignIn,
  faImage,
  faUser,
  faBell,
  faAngleLeft,
  faAngleRight,
  faArrowLeft,
  faTimes,
  faSearch,
  faList,
  faUserAlt,
  faPhone
);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}>
          <Route index element={<Home />} />
          <Route path="/" element={<ProtectedRoute />}>
            <Route path="profile" element={<Profile />} />
            <Route path="productinfo" element={<InfoProduk />} />
            <Route path="daftarjual" element={<ListProduct />} />
            <Route path="infopenawar" element={<InfoPenawar />} />
          </Route>
          <Route path="productpage/:id" element={<ProductPage />} />
        </Route>
        <Route path="/signup" element={<Signup />} />
        <Route path="/login" element={<Login />} />
        <Route path="/verifyemail" element={<Verification />} />
      </Routes>
    </BrowserRouter>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
