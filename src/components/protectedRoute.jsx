import React from "react";
import axios from "axios";
import Cookies from "js-cookie";
import jwtDecode from "jwt-decode";
import { login } from "../app/features/userSlice";
import { Navigate, Outlet } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { useState } from "react";

const ProtectedRoute = ({ path, exact, children }) => {
  const auth = useSelector((state) => state.user);
  const [loggedIn, setLoggedIn] = useState(auth.loggedIn);
  const dispatch = useDispatch();

  const isLoggedIn = async () => {
    const getToken = Cookies.get("token");
    if (getToken) {
      const decode = jwtDecode(getToken);
      axios.defaults.headers.common["Authorization"] = "Bearer " + getToken;
      try {
        const response = await axios.get(`/api/profile/${decode.id}`);
        const data = response.data;
        setLoggedIn(true);
        dispatch(login({ token: getToken, data: data.data }));
      } catch (error) {
        console.log(error);
        setLoggedIn(false);
      }
    }
  };

  useEffect(() => {
    if (!loggedIn) {
      isLoggedIn();
    }
  }, [loggedIn]);

  return loggedIn ? <Outlet /> : <Navigate to={"/login"} />;
};

export default ProtectedRoute;
