import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const getCategories = createAsyncThunk(
  "categories/getCategories",
  async () => {
    const response = await axios.get("api/categories");

    return response.data;
  }
);

const categoriesSlice = createSlice({
  name: "categories",
  initialState: {
    categories: [],
    status: null,
  },
  extraReducers: {
    [getCategories.pending]: (state, action) => {
      state.status = "loading";
    },
    [getCategories.fulfilled]: (state, { payload }) => {
      state.categories = payload;
      state.status = "success";
    },
    [getCategories.rejected]: (state, action) => {
      state.status = "failed";
    },
  },
});

export default categoriesSlice.reducer;
