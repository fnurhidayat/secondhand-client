import { useDispatch } from "react-redux";
import { Outlet } from "react-router";
import { clearSelected } from "./app/features/productsSlice";
import Header from "./components/header";

function App() {
  const dispatch = useDispatch();

  dispatch(clearSelected());
  return (
    <div>
      <Header />
      <Outlet />
    </div>
  );
}

export default App;
